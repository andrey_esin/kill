#!/bin/sh

u_id=$( id | grep 'uid=0' | wc -l )
if [ $u_id -eq 0 ]; then
  echo "Fail";
  echo "Use root";
  exit 1;
fi

mkdir /usr/local/share/kill_shoot
cp `pwd`/shoot.mp3 /usr/local/share/kill_shoot
cp `pwd`/kill /usr/local/bin/kill_shoot

echo "\nalias kill='/usr/local/bin/kill_shoot'" >> ~/.bashrc
alias kill='/usr/local/bin/kill_shoot'
